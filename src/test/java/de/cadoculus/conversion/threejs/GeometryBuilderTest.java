/*
 * CADOculus, 3D in the Web 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the MIT Licens as published at
 * http://opensource.org/licenses/mit-license.php
 * 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the MIT Licens
 * along with this program.  If not, see <* http://opensource.org/licenses/mit-license.php>
 *
 */
package de.cadoculus.conversion.threejs;

import de.cadoculus.conversion.threejs.GeometryBuilder;
import junit.framework.TestCase;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.PropertyConfigurator;
import static org.junit.Assert.*;
import org.junit.BeforeClass;
import org.junit.Test;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import javax.vecmath.Tuple3d;
import javax.vecmath.Vector3d;


/**
 * @author  cz
 */
public class GeometryBuilderTest {

    private static Log log = LogFactory.getLog( GeometryBuilderTest.class );

    @BeforeClass public static void setUpBeforeClass() throws Exception {
        BasicConfigurator.resetConfiguration();

        File logconfig = new File( "log4j.properties" );

        System.out.println( "check for " + logconfig.getAbsolutePath() );

        if ( logconfig.exists() && logconfig.canRead() ) {

            try {
                URL propsURL = new URL( "file:" + logconfig.getName() );
                PropertyConfigurator.configure( propsURL );
                log.info( "using log4j configuration from " + propsURL.toExternalForm() );
            } catch ( MalformedURLException ex ) {

                BasicConfigurator.resetConfiguration();
                BasicConfigurator.configure();

            }
        }
    }

    /**
     * Test of addTriangle method, of class GeometryBuilder.
     */
    @Test public void test001() throws IOException {

        Tuple3d v1 = new Vector3d( -0.5, -0.5, 1.0 );
        Tuple3d v2 = new Vector3d( 0.5, -0.5, 1.0 );
        Tuple3d v3 = new Vector3d( 0.5, 0.5, 1.0 );
        GeometryBuilder instance = new GeometryBuilder();
        instance.addTriangle( v1, v2, v3 );

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        instance.toJSON( bos );

        String result = new String( bos.toByteArray() );
        String ref = "{\n" + //
            "  \"metadata\": {\n" + //
            "     \"formatVersion\":3,\n" + //
            "     \"generatedBy\":\"GeometryBuilder\",\n" + //
            "     \"vertices\":3,\n" + //
            "     \"faces\":3,\n" + //
            "     \"normals\":0,\n" + //
            "     \"colors\":0,\n" + //
            "     \"uvs\":0,\n" + //
            "     \"materials\":0,\n" + //
            "     \"morphTargets\":0\n" + //
            "  },\n" + //
            "  \"scale\":1,\n" + //
            "  \"materials\":[],\n" + //
            "  \"vertices\":[-0.5,-0.5,1.0, 0.5,-0.5,1.0, 0.5,0.5,1.0],\n" + //
            "  \"morphTargets\":[],\n" + //
            "  \"morphColors\":[],\n" + //
            "  \"normals\":[],\n" + //
            "  \"colors\":[],\n" + //
            "  \"uvs\":[[]],\n" + //
            "  \"faces\":[0, 0,1,2]\n" + //
            "}\n";
        log.info( result );
        
        assertEquals( "result", ref, result);

    }
}
