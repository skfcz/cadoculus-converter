/*
 * CADOculus, 3D in the Web
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the MIT Licens as published at
 * http://opensource.org/licenses/mit-license.php
 *
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the MIT Licens
 * along with this program.  If not, see <* http://opensource.org/licenses/mit-license.php>
 *
 */
package de.cadoculus.converter;

import de.cadoculus.conversion.ConversionContext;
import de.cadoculus.conversion.impl.ConversionServiceImpl;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.PosixParser;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.jackrabbit.JcrConstants;
import org.apache.jackrabbit.core.TransientRepository;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.PropertyConfigurator;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import javax.jcr.Binary;
import javax.jcr.Node;
import javax.jcr.Property;
import javax.jcr.PropertyIterator;
import javax.jcr.PropertyType;
import javax.jcr.Repository;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.SimpleCredentials;

/**
 * This class is a command line converter using the "de.cadoculus.conversion"
 * mimic. It reads the input file into a JCR repository, runs the conversion and
 * copies the converted file into the target directory.
 */
public class CMDLineConverter {

    private static Log log = LogFactory.getLog( CMDLineConverter.class );

    public static void main( String[] args ) {

        BasicConfigurator.configure();

        Option helpO = new Option( "h", "help", false, "display help" );
        Option logO = OptionBuilder.withLongOpt( "log" ).hasArg().withDescription(
                "url for log4j.propertied file" ).create( "log" );
        Option inO = OptionBuilder.withArgName( "f" ).withLongOpt( "step" ).hasArg().isRequired(
                true ).withDescription( "the name of the step file" ).create( "f" );
        Option outO = OptionBuilder.withArgName( "d" ).withLongOpt( "dir" ).hasArg().isRequired(
                false ).withDescription(
                "the name of the output directory, default to current working directory" ).create(
                "d" );
        Option repO = OptionBuilder.withArgName( "r" ).withLongOpt( "repo" ).hasArg().isRequired(
                false ).withDescription(
                "the directory for the JCR repository, default to the systems temporary directory" )
                .create( "r" );

        Options options = new Options();
        options.addOption( helpO );
        options.addOption( logO );
        options.addOption( inO );
        options.addOption( outO );
        options.addOption( repO );

        // create the command line parser

        CommandLine line = null;

        try {
            CommandLineParser cparser = new PosixParser();

            // parse the command line arguments
            line = cparser.parse( options, args );
        } catch ( org.apache.commons.cli.ParseException exp ) {

            // oops, something went wrong
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp( "StepFormater", options );

            System.exit( 66 );
        }

        if ( line.hasOption( "h" ) ) {
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp( "StepFormater", options );
            System.exit( 0 );
        }

        // Log4j
        try {
            URL propsURL = CMDLineConverter.class.getResource( "defaultLog4j.properties" );

            if ( line.hasOption( "log" ) ) {
                propsURL = new URL( line.getOptionValue( "log" ) );
            }

            System.out.println( "using log4j configuration from " + propsURL.toExternalForm() );
            log.info( "using log4j configuration from " + propsURL.toExternalForm() );
            PropertyConfigurator.configure( propsURL );

        } catch ( MalformedURLException ex ) {
            System.err.println( "invalid properties url " + line.getOptionValue( "log" ) );
            System.err.println( "use standard configuration" );
            BasicConfigurator.resetConfiguration();
        }

        // Input
        File file = new File( line.getOptionValue( "f" ) );

        if ( !( file.isFile() && file.canRead() ) ) {
            log.error( "failed to open " + file.getAbsolutePath() );
            System.err.println( "got invalid file '" + file.getName() + "'" );

            System.exit( 66 );
        }

        // Repository directory
        File rep = line.hasOption( "r" )
                ? new File( line.getOptionValue( "r" ) )
                : new File( new File( System.getProperty( "java.io.tmpdir" ) ), "cadocrep" );
        rep.mkdirs();

        if ( !( rep.isDirectory() && rep.canWrite() ) ) {
            log.error( "given directory '" + rep.getAbsolutePath()
                    + "' is write protected.\nUse --repo to give a writable directory" );

            System.exit( 66 );
        }

        // Target Directory
        File out = line.hasOption( "o" ) ? new File( line.getOptionValue( "o" ) ) : new File( "." );

        if ( !( out.isDirectory() && out.canWrite() ) ) {
            log.error( "given directory '" + out.getAbsolutePath()
                    + "' is write protected.\nUse -d to give a writable directory" );

            System.exit( 66 );
        }

        // Start Jackrabit and import the given file
        Repository repository = null;
        Session session = null;
        Node inputFile = null;

        try {
            repository = new TransientRepository( rep );
            session = repository.login( new SimpleCredentials( System.getProperty( "user.name" ),
                                                               "password".toCharArray() ) );

            String user = session.getUserID();
            String name = repository.getDescriptor( Repository.REP_NAME_DESC );
            System.out.println( "Logged in as " + user + " to a " + name + " repository." );

            Node root = session.getRootNode();

            log.info( "root " + root );

            Node inputFolder = root.hasNode( "input" )
                    ? root.getNode( "input" ) : root.addNode( "input", JcrConstants.NT_FOLDER );

            InputStream fis = new FileInputStream( file );
            inputFile = org.apache.jackrabbit.commons.JcrUtils.putFile( inputFolder, file.getName(),
                                                                        "application/octet", fis );

            session.save();

            // Use the simple ConversioService implementation

            Node outputFiles = root.addNode( "output", JcrConstants.NT_FOLDER );

            ConversionServiceImpl csi = new ConversionServiceImpl( repository, session,
                                                                   outputFiles );

            csi.convert( inputFile );

            ConversionContext ctx = csi.getConversionContext();

            log.info( "converted " + inputFile.getName() + "into " );
            log.info( "product structures " + ctx.getConvertedStructures() );
            log.info( "   root " + ctx.getConvertedRootStructure() );
            log.info( "geometries " + ctx.getConvertedGeometries() );


            Set< Node> result = new HashSet<Node>();
            result.addAll( ctx.getConvertedGeometries() );
            result.addAll( ctx.getConvertedStructures() );


            String key = "jcr:content/{http://www.jcp.org/jcr/1.0}data";




            byte[] buffer = new byte[1024];
            int numBytes = 0;

            // export from JCR to file system
            for ( Iterator<Node> it = result.iterator(); it.hasNext(); ) {
                Node resultingFile = it.next();

                log.info( "copy " + resultingFile.getName() + " to " + out );

                File resultFile = new File( out, resultingFile.getName() );
                Binary data = resultingFile.getProperty( key ).getBinary();
                FileOutputStream fos = new FileOutputStream( resultFile );
                InputStream is = data.getStream();

                while ( ( numBytes = is.read( buffer ) ) >= 0 ) {
                    fos.write( buffer, 0, numBytes );
                }

                fos.close();
                is.close();

            }


        } catch ( Exception exp ) {
            log.error( "error", exp );

        } finally {
            session.logout();
        }
    }

    private static void printNode( Node f ) throws RepositoryException {
        log.info( "f " + f.getName() + "; " + Arrays.toString( f.getMixinNodeTypes() ) );
        log.info( "   type " + f.getPrimaryNodeType().getPrimaryItemName() );
        log.info( "   path " + f.getPath() );
        log.info( "    def " + f.getDefinition().getDefaultPrimaryTypeName() );

        for ( PropertyIterator it = f.getProperties(); it.hasNext(); ) {
            Property object = it.nextProperty();

            switch ( object.getType() ) {

                case PropertyType.BINARY: {
                    log.info( "    P Binary " + object.getName() );

                    break;
                }

                case PropertyType.DATE: {
                    log.info( "    P Date " + object.getName() + " :  " + object.getDate() );

                    break;
                }

                case PropertyType.STRING: {
                    log.info( "    P String " + object.getName() + ": " + object.getString() );

                    break;
                }

                case PropertyType.NAME: {
                    log.info( "    P Name " + object.getName() + ": "
                            + object.getDefinition().getName() );

                    break;
                }

                default: {
                    log.info( "    P unknown type " + object.getType() + " " + object.getName() );
                }
            }

        }
    }
}
