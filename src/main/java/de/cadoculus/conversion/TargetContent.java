/*
 * CADOculus, 3D in the Web
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the MIT Licens as published at
 * http://opensource.org/licenses/mit-license.php
 *
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the MIT Licens
 * along with this program.  If not, see <* http://opensource.org/licenses/mit-license.php>
 *
 */
package de.cadoculus.conversion;

/**
 * Enumeration used to tell the converters what type of content to create
 *
 * @author  cz
 */
public enum TargetContent {

    /**
     * Create only geometry json
     */
    GEOMETRY,

    /**
     * Create only Scenegraph json
     */
    SCENE,

    /**
     * Create both geometry and scenegraph json
     */
    GEOMETRY_AND_SCENE,

    /**
     * Create geometry json, scenegraph only created if input file contains internal product
     * structure
     */
    GEOMETRY_SCENE_ONLY_IF_FOUND,

}
