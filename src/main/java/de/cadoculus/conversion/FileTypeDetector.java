/*
 * CADOculus, 3D in the Web 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the MIT Licens as published at
 * http://opensource.org/licenses/mit-license.php
 * 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the MIT Licens
 * along with this program.  If not, see <* http://opensource.org/licenses/mit-license.php>
 *
 */
package de.cadoculus.conversion;

/**
 * This class is used to detect the type of File.
 *
 * @author  cz
 */
public interface FileTypeDetector {

    /**
     * The getConverter method is called on all instances of the FileTypeDetector. If the
     * implementation of the FileTypeDetector detects the type and has an associated FileConverter,
     * it returns an instance of the converte. Otherwise it returns null.
     *
     * @param   finfo  a preliminary filled instance of the node. the mimetype attribute should be
     *                 set by the detector on the finfo node
     *
     * @return  a FileConvert or null
     */
    FileConverter getConverter( javax.jcr.Node finfo ) throws javax.jcr.RepositoryException;

}
