/*
 * CADOculus, 3D in the Web
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the MIT Licens as published at
 * http://opensource.org/licenses/mit-license.php
 *
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the MIT Licens
 * along with this program.  If not, see <* http://opensource.org/licenses/mit-license.php>
 *
 */
package de.cadoculus.conversion;

/**
 * An enumeration for the conversion lifecycle.<br>
 * See <a href="package.html">here</a> for more explantion
 *
 * @author  cz
 */
public enum ConversionStatus {

    START, UPLOADING, UPLOADED, DETECTING_FILE_TYPE, DETECTED_FILE_TYPE, FOUND_MISSING_FILE,
    FOUND_UNSUPPORTED_FORMAT, CONVERTING, CONVERTED, DELETING, DELETED, FINISH
}
