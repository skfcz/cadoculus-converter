/*
 * CADOculus, 3D in the Web
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the MIT Licens as published at
 * http://opensource.org/licenses/mit-license.php
 *
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the MIT Licens
 * along with this program.  If not, see <* http://opensource.org/licenses/mit-license.php>
 *
 */
package de.cadoculus.conversion;

import java.util.Set;


/**
 * Thrown if a file was referenced which is not contained in the incomming files
 *
 * @author  cz
 */
public class MissingFilesException extends ConversionException {
    private Set< String > names;

    /**
     * Constructs an instance of <code>ConversionException</code> with the specified detail message.
     *
     * @param  msg  the detail message.
     */
    public MissingFilesException( String msg, Set< String > names ) {
        super( msg );
        this.names = names;
    }

    /**
     * Get a list with missing file names
     *
     * @return  missing file names
     */
    public Set< String > getMissingFileNames() {
        return this.names;
    }
}
