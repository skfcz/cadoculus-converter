package de.cadoculus.conversion.impl.stl;

/*
   Copyright 2001  Universidad del Pais Vasco (UPV/EHU)

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Reader;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.ByteBuffer;

// New from JDK 1.4 for endian related problems
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.StringTokenizer;
import javax.vecmath.Point3f;
import javax.vecmath.Vector3f;


/**
 * Title: STL Loader Description: STL files loader (Supports ASCII and binary files) for Java3D
 * Needs JDK 1.4 due to endian problems Company: Universidad del Pais Vasco (UPV/EHU)
 *
 * @author:   Carlos Pedrinaci Godoy Company: National Aeronautics and Space Administration, Inlets
 *            and Nozzles (NASA/RTE0)
 * @author:   Avinash Devalla
 * @version:  1.0 Contact : xenicp@yahoo.es Contact: avinash.s.devalla@gmail.com Things TO-DO: 1.-We
 *            can't read binary files over the net. 2.-For binary files if size is lower than
 *            expected (calculated with the number of faces) the program will block. 3.-Improve the
 *            way for detecting the kind of stl file? Can give us problems if the comment of the
 *            binary file begins by "solid"
 */

public class StlFile {

    // Maximum length (in chars) of basePath
    private static final int MAX_PATH_LENGTH = 1024;
    private static Log log = LogFactory.getLog( StlFile.class );
    private boolean Ascii = true; // File type Ascii -> true o binary -> false
    private String basePath; // For local files

    private URL baseUrl; // Reading files over Internet

    // GeometryInfo needs Arrays
    private Point3f[] coordArray;

    // Arrays with coordinates and normals
    // Needed for reading ASCII files because its size is unknown until the end
    private ArrayList< Point3f > coordList; // Holds Point3f
    private String fileName;

    // Global variables
    private int flag; // Needed cause implements Loader

    private boolean fromUrl = false; // Usefull for binary files
    private Vector3f[] normArray;
    private ArrayList< Vector3f > normList; // Holds Vector3f

    // Default = Not available
    private String objectName = new String( "Not available" );

    // Needed because TRIANGLE_STRIP_ARRAY
    // As the number of strips = the number of faces it's filled in objectToVectorArray
    private int[] stripCounts;

    /**
     * Constructor
     */
    public StlFile() {
    }

    /**
     * The Stl File is loaded from the already opened file. To attach the model to your scene, call
     * getSceneGroup() on the Scene object passed back, and attach the returned BranchGroup to your
     * scene graph. For an example, see $J3D/programs/examples/ObjLoad/ObjLoad.java.
     *
     * @param   reader  The reader to read the object from
     *
     * @return  Scene The scene with the object loaded.
     *
     * @throws  FileNotFoundException
     * @throws  IncorrectFormatException
     * @throws  ParsingErrorException
     */
    public void load( Reader reader ) throws IOException {
        // That method calls the method that loads the file for real..
        // Even if the Stl format is not complicated I've decided to use
        // a parser as in the Obj's loader included in Java3D

        StlFileParser st = new StlFileParser( reader );

        // Initialize data
        coordList = new ArrayList< Point3f >();
        normList = new ArrayList< Vector3f >();
        setAscii( true ); // Default ascii
        readFile( st );

    }

    public boolean getAscii() {
        return this.Ascii;
    }

    public String getBasePath() {
        return basePath;
    }

    /**
     * Accessors and Modifiers
     */

    public URL getBaseUrl() {
        return baseUrl;
    }

    /**
     * @return  the coordList
     */
    public ArrayList< Point3f > getCoordList() {
        return coordList;
    }

    public String getFileName() {
        return this.fileName;
    }

    public int getFlags() {
        return flag;
    }

    /**
     * @return  the normList
     */
    public ArrayList< Vector3f > getNormList() {
        return normList;
    }

    public String getObjectName() {
        return this.objectName;
    }

    public void setAscii( boolean tipo ) {
        this.Ascii = tipo;
    }

    /**
     * Set the path where files associated with this .stl file are located. Only needs to be called
     * to set it to a different directory from that containing the .stl file.
     *
     * @param  pathName  The new Path to the file
     */
    public void setBasePath( String pathName ) {
        basePath = pathName;

        if ( ( basePath == null ) || ( basePath == "" ) ) {
            basePath = "." + java.io.File.separator;
        }

        basePath = basePath.replace( '/', java.io.File.separatorChar );
        basePath = basePath.replace( '\\', java.io.File.separatorChar );

        if ( !basePath.endsWith( java.io.File.separator ) ) {
            basePath = basePath + java.io.File.separator;
        }
    } // End of setBasePath

    /**
     * Modifier for baseUrl, if accessing internet.
     *
     * @param  url  The new url
     */
    public void setBaseUrl( URL url ) {
        baseUrl = url;
    }

    public void setFileName( String filename ) {
        this.fileName = new String( filename );
    }

    public void setFlags( int parm ) {
        this.flag = parm;
    }

    public void setObjectName( String name ) {
        this.objectName = name;
    }

    /**
     * Method that reads normal, vertex coordinates; stores them as Point3f. Written by adevalla
     *
     * @param  parser    - file parser, instance of StlFileParser
     * @param  parseKey  - string "normal", "vertex"
     */

    private void read3d( StlFileParser parser, String parseKey ) {
        float x, y, z;

        if ( !( ( parser.ttype == StlFileParser.TT_WORD ) && parser.sval.equals( parseKey ) ) ) {
            System.err.println( "Format Error:expecting '" + parseKey + "' on line " +
                parser.lineno() );
        } else {

            if ( parser.getNumber() ) {
                x = ( float ) parser.nval;

                if ( parser.getNumber() ) {
                    y = ( float ) parser.nval;

                    if ( parser.getNumber() ) {
                        z = ( float ) parser.nval;

                        // We add that vertex to the array of vertex

                        if ( "vertex".equals( parseKey ) ) {
                            Point3f vertex = new Point3f( x, y, z );
                            //log.info( "read vertex " + vertex);
                            coordList.add( vertex );
                        } else if ( "normal".equals( parseKey ) ) {
                            Vector3f normal = new Vector3f( x, y, z );
                            //log.info("read normal " + normal);
                            normList.add( normal );
                        } else {
                            throw new IllegalStateException("expect one of 'vertex' or 'normal', got " + parseKey);
                        }

                        readEOL( parser );
                    } else {
                        System.err.println( "Format Error: expecting coordinate on line " +
                            parser.lineno() );
                    }
                } else {
                    System.err.println( "Format Error: expecting coordinate on line " +
                        parser.lineno() );
                }
            } else {
                System.err.println( "Format Error: expecting coordinate on line " +
                    parser.lineno() );
            }
        }
    }

    /**
     * Method for reading binary files Execution is completly different It uses ByteBuffer for
     * reading data and ByteOrder for retrieving the machine's endian (Needs JDK 1.4) TO-DO: 1.-Be
     * able to read files over Internet 2.-If the amount of data expected is bigger than what is on
     * the file then the program will block forever
     *
     * @param   file  The name of the file
     *
     * @throws  IOException
     */
    private void readBinaryFile( String file ) throws IOException {
        FileInputStream data; // For reading the file
        ByteBuffer dataBuffer; // For reading in the correct endian
        byte[] Info = new byte[ 80 ]; // Header data
        byte[] Array_number = new byte[ 4 ]; // Holds the number of faces
        byte[] Temp_Info; // Intermediate array

        int Number_faces; // First info (after the header) on the file

        // Get file's name
        if ( fromUrl ) {

            // FileInputStream can only read local files!?
            System.out.println( "This version doesn't support reading binary files from internet" );
        } else { // It's a local file
            data = new FileInputStream( file );

            // First 80 bytes aren't important
            if ( 80 != data.read( Info ) ) { // File is incorrect

                //System.out.println("Format Error: 80 bytes expected");
                throw new IllegalStateException( "Format Error: 80 bytes expected" );
            } else { // We must first read the number of faces -> 4 bytes int
                     // It depends on the endian so..

                data.read( Array_number ); // We get the 4 bytes
                dataBuffer = ByteBuffer.wrap( Array_number ); // ByteBuffer for reading correctly the int
                dataBuffer.order( ByteOrder.nativeOrder() ); // Set the right order
                Number_faces = dataBuffer.getInt();

                Temp_Info = new byte[ 50 * Number_faces ]; // Each face has 50 bytes of data

                data.read( Temp_Info ); // We get the rest of the file

                dataBuffer = ByteBuffer.wrap( Temp_Info ); // Now we have all the data in this ByteBuffer
                dataBuffer.order( ByteOrder.nativeOrder() );

                // We can create that array directly as we know how big it's going to be
                coordArray = new Point3f[ Number_faces * 3 ]; // Each face has 3 vertex
                normArray = new Vector3f[ Number_faces ];
                stripCounts = new int[ Number_faces ];

                for ( int i = 0; i < Number_faces; i++ ) {
                    stripCounts[ i ] = 3;

                    try {
                        readFacetB( dataBuffer, i );

                        // After each facet there are 2 bytes without information
                        // In the last iteration we dont have to skip those bytes..
                        if ( i != ( Number_faces - 1 ) ) {
                            dataBuffer.get();
                            dataBuffer.get();
                        }
                    } catch ( IOException e ) {

                        // Quitar
                        System.out.println( "Format Error: iteration number " + i );
                        throw new IllegalStateException( "Format Error: iteration number " + i );
                    }
                } //End for
            } // End file reading
        } // End else
    } // End of readBinaryFile

    /**
     * Method that reads the EOL Needed for verifying that the file has a correct format
     *
     * @param  parser  The file parser. An instance of StlFileParser.
     */
    private void readEOL( StlFileParser parser ) {

        try {
            parser.nextToken();
        } catch ( IOException e ) {
            e.printStackTrace();
        }

        if ( parser.ttype != StlFileParser.TT_EOL ) {
            System.err.println( "Format Error:expecting End Of Line on line " + parser.lineno() );
        }
    }

    /**
     * Method that reads a face of the object. (Cares about the format).
     *
     * @param  parser  The file parser. An instance of StlFileParser.
     */
    private void readFacet( StlFileParser parser ) {

        if ( ( parser.ttype != StlFileParser.TT_WORD ) || !parser.sval.equals( "facet" ) ) {
            System.err.println( "Format Error:expecting 'facet' on line " + parser.lineno() );
        } else {

            try {
                parser.nextToken();
                read3d( parser, "normal" );

                parser.nextToken();
                readToken( parser, "outer" );

                for ( int i = 0; i < 3; i++ ) //reads three vertices
                {
                    parser.nextToken();
                    read3d( parser, "vertex" );
                }

                parser.nextToken();
                readToken( parser, "endloop" );

                parser.nextToken();
                readToken( parser, "endfacet" );
            } catch ( IOException e ) {
                System.err.println( "IO Error on line " + parser.lineno() + ": " + e.getMessage() );
            }
        }
    } // End of readFacet

    /**
     * Method that reads a face in binary files All binary versions of the methods end by 'B' As in
     * binary files we can read the number of faces, we don't need to use coordArray and normArray
     * (reading binary files should be faster)
     *
     * @param   in     The ByteBuffer with the data of the object.
     * @param   index  The facet index
     *
     * @throws  IOException
     */
    private void readFacetB( ByteBuffer in, int index ) throws IOException {

        // Read the Normal
        normArray[ index ] = new Vector3f( in.getFloat(), in.getFloat(), in.getFloat() );

        // Read vertex1
        coordArray[ index * 3 ] = new Point3f( in.getFloat(), in.getFloat(), in.getFloat() );

        // Read vertex2
        coordArray[ ( index * 3 ) + 1 ] = new Point3f( in.getFloat(), in.getFloat(),
                in.getFloat() );

        // Read vertex3
        coordArray[ ( index * 3 ) + 2 ] = new Point3f( in.getFloat(), in.getFloat(),
                in.getFloat() );

    } // End of readFacetB

    /**
     * Method that reads ASCII files Uses StlFileParser for correct reading and format checking The
     * beggining of that method is common to binary and ASCII files We try to detect what king of
     * file it is TO-DO: 1.- Find a best way to decide what kind of file it is 2.- Is that return
     * (first catch) the best thing to do?
     *
     * @param  parser  The file parser. An instance of StlFileParser.
     */
    private void readFile( StlFileParser parser ) {

        try {
            parser.nextToken();
        } catch ( IOException e ) {
            System.err.println( "IO Error on line " + parser.lineno() + ": " + e.getMessage() );
            System.err.println( "File seems to be empty" );

            return; // ????? Throw ?????
        }

        // Here we try to detect what kind of file it is (see readSolid)
        readSolid( parser );

        if ( getAscii() ) { // Ascii file

            try {
                parser.nextToken();
            } catch ( IOException e ) {
                System.err.println( "IO Error on line " + parser.lineno() + ": " + e.getMessage() );
            }

            // Read all the facets of the object
            while ( ( parser.ttype != StlFileParser.TT_EOF ) &&
                    !parser.sval.equals( "endsolid" ) ) {
                readFacet( parser );

                try {
                    parser.nextToken();
                } catch ( IOException e ) {
                    System.err.println( "IO Error on line " + parser.lineno() + ": " +
                        e.getMessage() );
                }
            } // End while

            // Why are we out of the while?: EOF or endsolid
            if ( parser.ttype == StlFileParser.TT_EOF ) {
                System.err.println( "Format Error:expecting 'endsolid', line " + parser.lineno() );
            }
        } //End of Ascii reading
        else { // Binary file

            try {
                readBinaryFile( getFileName() );
            } catch ( IOException e ) {
                System.err.println( "Format Error: reading the binary file" );
            }
        } // End of binary file
    } //End of readFile

    /**
     * Method that reads the word "solid" and stores the object name. It also detects what kind of
     * file it is TO-DO: 1.- Better way control of exceptions? 2.- Better way to decide between
     * Ascii and Binary?
     *
     * @param  parser  The file parser. An instance of StlFileParser.
     */
    private void readSolid( StlFileParser parser ) {

        if ( !parser.sval.equals( "solid" ) ) {

            //System.out.println("Expecting solid on line " + parser.lineno());
            // If the first word is not "solid" then we consider the file is binary
            // Can give us problems if the comment of the binary file begins by "solid"
            this.setAscii( false );
        } else // It's an ASCII file
        {

            try {
                parser.nextToken();
            } catch ( IOException e ) {
                System.err.println( "IO Error on line " + parser.lineno() + ": " + e.getMessage() );
            }

            if ( parser.ttype != StlFileParser.TT_WORD ) {

                // Is the object name always provided???
                System.err.println( "Format Error:expecting the object name on line " +
                    parser.lineno() );
            } else {

                // Store the object Name
                this.setObjectName( new String( parser.sval ) );
                this.readEOL( parser );
            }
        }
    } //End of readSolid

    /**
     * Method that reads tokens and allows iteration through facet definition Written by adevalla
     *
     * @param  parser    - file parser, instance of StlFileParser
     * @param  parseKey  - string "outer", "loop", "endloop", or "endfacet"
     */

    private void readToken( StlFileParser parser, String parseKey ) {

        if ( ( parser.ttype != StlFileParser.TT_WORD ) || !parser.sval.equals( parseKey ) ) {
            System.err.println( "Format Error:expecting " + parseKey + " on line " +
                parser.lineno() );
        } else {

            if ( parseKey.equals( "outer" ) ) {

                try {
                    parser.nextToken();
                } catch ( IOException e ) {

                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }

            readEOL( parser );
        }
    }

    /**
     * Takes a file name and sets the base path to the directory containing that file.
     */
    private void setBasePathFromFilename( String fileName ) {

        // Get ready to parse the file name
        StringTokenizer stok = new StringTokenizer( fileName, java.io.File.separator );

        //  Get memory in which to put the path
        StringBuffer sb = new StringBuffer( MAX_PATH_LENGTH );

        // Check for initial slash
        if ( ( fileName != null ) && fileName.startsWith( java.io.File.separator ) ) {
            sb.append( java.io.File.separator );
        }

        // Copy everything into path except the file name
        for ( int i = stok.countTokens() - 1; i > 0; i-- ) {
            String a = stok.nextToken();
            sb.append( a );
            sb.append( java.io.File.separator );
        }

        setBasePath( sb.toString() );
    } // End of setBasePathFromFilename

    private void setBaseUrlFromUrl( URL url ) {
        StringTokenizer stok = new StringTokenizer( url.toString(), "/\\", true );
        int tocount = stok.countTokens() - 1;
        StringBuffer sb = new StringBuffer( MAX_PATH_LENGTH );

        for ( int i = 0; i < tocount; i++ ) {
            String a = stok.nextToken();
            sb.append( a );
//      if((i == 0) && (!a.equals("file:"))) {
//          sb.append(a);
//          sb.append(java.io.File.separator);
//          sb.append(java.io.File.separator);
//      } else {
//          sb.append(a);
//          sb.append( java.io.File.separator );
//      }
        }

        try {
            baseUrl = new URL( sb.toString() );
        } catch ( MalformedURLException e ) {
            System.err.println( "Error setting base URL: " + e.getMessage() );
        }
    } // End of setBaseUrlFromUrl

} // End of package stl_loader
