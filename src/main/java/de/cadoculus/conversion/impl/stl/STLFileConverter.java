/*
 * CADOculus, 3D in the Web
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the MIT Licens as published at
 * http://opensource.org/licenses/mit-license.php
 *
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the MIT Licens
 * along with this program.  If not, see <* http://opensource.org/licenses/mit-license.php>
 *
 */
package de.cadoculus.conversion.impl.stl;

import de.cadoculus.conversion.ConversionContext;
import de.cadoculus.conversion.ConversionException;
import de.cadoculus.conversion.FileConverter;
import de.cadoculus.conversion.TargetContent;
import de.cadoculus.conversion.threejs.GeometryBuilder;
import de.cadoculus.conversion.threejs.ThreeJsBinMesh;
import de.cadoculus.conversion.threejs.ThreeJsNode;
import de.cadoculus.conversion.threejs.ThreeJsScene;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import javax.jcr.Binary;
import javax.jcr.Node;
import javax.jcr.Property;
import javax.jcr.RepositoryException;
import javax.jcr.nodetype.NodeType;
import javax.vecmath.Point3f;


/**
 * @author  cz
 */
class STLFileConverter implements FileConverter {

    private static Log log = LogFactory.getLog( STLFileConverter.class );

    private ConversionContext ctx;
    private Node productStructure;
    private Set< Node > result = new HashSet< Node >();
    private Node targetNode;

    public STLFileConverter() {
    }

    @Override public void convert( javax.jcr.Node finfo ) throws RepositoryException,
        ConversionException {

        if ( finfo == null ) {
            throw new IllegalArgumentException( "expect none null javax.jcr.Node" );
        }

        String mimetype = finfo.getProperty( "jcr:content/jcr:mimeType" ).getString();

        if ( STLFileTypeDetector.MIME_TYPE.equals( mimetype ) ) {
            // OK
        } else {
            throw new IllegalArgumentException( "expect Mimetype  " +
                STLFileTypeDetector.MIME_TYPE + ", got '" + mimetype + "'" );
        }

        try {
            Node geometry = null;

            if ( ( ctx.getTargetContent() == TargetContent.GEOMETRY ) ||
                    ( ctx.getTargetContent() == TargetContent.GEOMETRY_AND_SCENE ) ||
                    ( ctx.getTargetContent() == TargetContent.GEOMETRY_SCENE_ONLY_IF_FOUND ) ) {
                StlFile sfile = new StlFile();

                Binary bin = finfo.getProperty( "jcr:content/{http://www.jcp.org/jcr/1.0}data" )
                    .getBinary();
                Reader reader = new InputStreamReader( bin.getStream() );
                sfile.load( reader );

                List< Point3f > vertexes = sfile.getCoordList();

                //log.info( "vertexes #" + vertexes.size() + " : " + vertexes );

                GeometryBuilder builder = new GeometryBuilder();

                for ( Iterator< Point3f > it = vertexes.iterator(); it.hasNext(); ) {
                    Point3f v1 = it.next();
                    Point3f v2 = it.next();
                    Point3f v3 = it.next();

                    builder.addTriangle( v1, v2, v3 );
                }

                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                builder.toJSON( bos );

                geometry = targetNode.addNode( finfo.getName() + ".json", NodeType.NT_FILE );

                Node content = geometry.addNode( Node.JCR_CONTENT, NodeType.NT_RESOURCE );
                content.setProperty( Property.JCR_MIMETYPE, "threejs/geometry/json" );

                byte[] data = bos.toByteArray();
                InputStream is = new ByteArrayInputStream( data );

                Binary binary = targetNode.getSession().getValueFactory().createBinary( is );
                content.setProperty( Property.JCR_DATA, binary );

                result.add( geometry );
                this.ctx.getConvertedGeometries().add( geometry );
            } else {
                throw new ConversionException(
                    "STL files do not contain product structure, TargetContent " +
                    ctx.getTargetContent() + " is questionable setting" );
            }

            // finished with geometry, now create a small scene as product structure if demanded
            if ( ( TargetContent.GEOMETRY_AND_SCENE == ctx.getTargetContent() ) ||
                    ( TargetContent.SCENE == ctx.getTargetContent() ) ) {

                ThreeJsScene scene = new ThreeJsScene();
                ThreeJsBinMesh mesh = scene.addGeometry( ThreeJsBinMesh.class );
                mesh.setRelativePath( geometry.getName() );

                ThreeJsNode node = scene.getRootNode();
                node.setGeometry( mesh );

                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                scene.toJSON( bos );
                bos.close();

                Node sceneGraph = targetNode.addNode( finfo.getName() + "_scene.json",
                        NodeType.NT_FILE );
                Node content = sceneGraph.addNode( Node.JCR_CONTENT, NodeType.NT_RESOURCE );
                content.setProperty( Property.JCR_MIMETYPE, "threejs/scene/json" );

                byte[] data = bos.toByteArray();
                InputStream is = new ByteArrayInputStream( data );

                Binary binary = targetNode.getSession().getValueFactory().createBinary( is );
                content.setProperty( Property.JCR_DATA, binary );

                result.add( sceneGraph );
                this.productStructure = sceneGraph;

            }

        } catch ( IOException ioexp ) {
            throw new ConversionException( "an IO error occured during conversion", ioexp );
        }

    }

    @Override public ConversionContext getConversionContext() {
        return ctx;
    }

    public Set< Node > getConvertedGeometries() {
        return result;
    }

    public Node getProductStructureNode() {
        return productStructure;
    }

    @Override public Node getTargetNode() {
        return targetNode;
    }

    @Override public void setConversionContext( ConversionContext ctx ) {
        this.ctx = ctx;
    }

    @Override public void setTargetNode( Node root ) {
        this.targetNode = root;
    }
}
