/*
 * CADOculus, 3D in the Web 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the MIT Licens as published at
 * http://opensource.org/licenses/mit-license.php
 * 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the MIT Licens
 * along with this program.  If not, see <* http://opensource.org/licenses/mit-license.php>
 *
 */
package de.cadoculus.conversion.impl.stl;

import de.cadoculus.conversion.FileConverter;
import de.cadoculus.conversion.FileTypeDetector;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Locale;
import javax.jcr.Binary;
import javax.jcr.Node;
import javax.jcr.Property;


/**
 * <pre>
 * solid MYSOLID created by IVREAD, original data in cube.iv
 * facet normal   0.000000E+00  0.000000E+00   1.00000
 * outer loop
 * vertex  -0.500000     -0.500000       1.00000
 * vertex   0.500000     -0.500000       1.00000
 * vertex   0.500000      0.500000       1.00000
 * vertex  -0.500000      0.500000       1.00000
 * endloop
 * endfacet
 * </pre>
 *
 * @author  cz
 */
public class STLFileTypeDetector implements FileTypeDetector {

    public static final String MIME_TYPE = "application/sla";

    Log log = LogFactory.getLog( STLFileTypeDetector.class );

    @Override public FileConverter getConverter( Node finfo ) throws javax.jcr.RepositoryException {

        // Check input
        if ( finfo == null ) {
            throw new IllegalArgumentException( "got null javax.jcr.Node object" );
        }

        String name = finfo.getName();

        if ( ( name == null ) || ( name.trim().length() == 0 ) ) {
            log.warn( "no name in javax.jcr.Node " + finfo );

            return null;
        }
        log.info("check " + name);

        // Get the children node with the binary content
        Node c = finfo.getNode( "jcr:content" );
        Binary bin = c.getProperty( Property.JCR_DATA ).getBinary();

        if ( bin == null ) {
            log.warn( "no content in javax.jcr.Node " + finfo );

            return null;
        }

        FileConverter retval = null;

        // Check local file name
        if ( !name.toLowerCase( Locale.ENGLISH ).endsWith( ".stl" ) ) {
            log.info( "file name does not end wit .stl, stop check" );

            return retval;
        }

        // Check if file is an ASCII STL file
        // First line has to start with 'solid'

        try {
            InputStream fis = bin.getStream();
            BufferedReader reader = new BufferedReader( new InputStreamReader( fis ) );
            String line = reader.readLine();

            if ( line.startsWith( "solid" ) ) {
                line = reader.readLine();

                if ( ( line != null ) && line.trim().startsWith( "facet normal" ) ) {
                    line = reader.readLine();

                    if ( ( line != null ) && line.trim().startsWith( "outer loop" ) ) {
                        // OK, file seems to be an STL ASCII file
                        // enhance javax.jcr.Node and return appropriate FileConverte

                        c.setProperty( Property.JCR_MIMETYPE, MIME_TYPE );

                        retval = new STLFileConverter();
                    }
                }

            }

            reader.close();

        } catch ( IOException exp ) {
            log.warn( "an error occured reading the file " + finfo );
        }

        return retval;

    }
}
