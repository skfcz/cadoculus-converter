/*
 * CADOculus, 3D in the Web
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the MIT Licens as published at
 * http://opensource.org/licenses/mit-license.php
 *
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the MIT Licens
 * along with this program.  If not, see <* http://opensource.org/licenses/mit-license.php>
 *
 */
package de.cadoculus.conversion.impl;

import de.cadoculus.conversion.ConversionContext;
import de.cadoculus.conversion.ConversionService;
import de.cadoculus.conversion.ConversionStatus;
import de.cadoculus.conversion.TargetContent;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;
import javax.jcr.Node;


/**
 * This class contains context information on a single conversion.
 *
 * @author  cz
 */
public class ConversionContextImpl implements ConversionContext {
    private Properties attributes;
    private ConversionService conversionService;
    private Set< javax.jcr.Node > convertedGeometries = new HashSet< javax.jcr.Node >();
    private Set< Node > convertedStructures = new HashSet< javax.jcr.Node >();
    private Set< String > missingFiles = new HashSet< String >();
    private javax.jcr.Node productStructure;
    private ConversionStatus status = ConversionStatus.START;
    private TargetContent targetContent = TargetContent.GEOMETRY_AND_SCENE;
    private javax.jcr.Node targetFolder;
    private Set< javax.jcr.Node > uploadedFiles = new HashSet< javax.jcr.Node >();
    private javax.jcr.Node uploadedRootFile;

    @Override public Properties getAttributes() {
        return attributes;
    }

    /**
     * @return  the conversionService
     */
    public ConversionService getConversionService() {
        return conversionService;
    }

    @Override public Set< javax.jcr.Node > getConvertedGeometries() {
        return convertedGeometries;
    }

    @Override public javax.jcr.Node getConvertedRootStructure() {
        return productStructure;
    }

    public Set< Node > getConvertedStructures() {
        return convertedStructures;
    }

    @Override public Set< String > getMissingFiles() {
        return missingFiles;
    }

    @Override public ConversionStatus getStatus() {
        return status;
    }

    public TargetContent getTargetContent() {
        return targetContent;
    }

    /**
     * @return  the targetFolder
     */
    public javax.jcr.Node getTargetFolder() {
        return targetFolder;
    }

    @Override public Set< javax.jcr.Node > getUploadedFiles() {
        return uploadedFiles;
    }

    @Override public javax.jcr.Node getUploadedRootFile() {
        return uploadedRootFile;
    }

    /**
     * @param  conversionService  the conversionService to set
     */
    public void setConversionService( ConversionService conversionService ) {
        this.conversionService = conversionService;
    }

    public void setConvertedRootStructure( javax.jcr.Node ps ) {
        productStructure = ps;
    }

    @Override public void setStatus( ConversionStatus status ) {
        this.status = status;
    }

    public void setTargetContent( TargetContent tc ) {
        this.targetContent = tc;
    }

    /**
     * @param  targetFolder  the targetFolder to set
     */
    public void setTargetFolder( javax.jcr.Node targetFolder ) {
        this.targetFolder = targetFolder;
    }

    public void setUploadedRootFile( javax.jcr.Node uploadedRootFile ) {
        this.uploadedRootFile = uploadedRootFile;
    }

}
