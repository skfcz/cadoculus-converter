/*
 * CADOculus, 3D in the Web
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the MIT Licens as published at
 * http://opensource.org/licenses/mit-license.php
 *
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the MIT Licens
 * along with this program.  If not, see <* http://opensource.org/licenses/mit-license.php>
 *
 */
package de.cadoculus.conversion;

import java.io.IOException;
import java.util.Set;
import javax.jcr.Node;
import javax.jcr.RepositoryException;


/**
 * The FileConverter is used to convert one input file into 1 ... n output files.
 *
 * @author  cz
 */
public interface FileConverter {

    /**
     * Start the conversion of the given content to one or more result files.
     *
     * @param   finfo  the file to convert
     *
     * @return  a Set of result files
     */
    public void convert( javax.jcr.Node finfo ) throws RepositoryException, ConversionException;

    /**
     * Get the context of the conversion
     *
     * @return  ctx the ConversionContext
     */
    public ConversionContext getConversionContext();

    /**
     * Get the converted result, includes all kind of files
     */
    public Set< Node > getConvertedGeometries();

    /**
     * Get the converted product structure file
     *
     * @return
     */
    public Node getProductStructureNode();

    /**
     * Get node under to which converted content should be added
     *
     * @return  the root node for the conversion output
     */
    public javax.jcr.Node getTargetNode();

    /**
     * Set the context of the conversion
     *
     * @param  ctx  the ConversionContext
     */
    public void setConversionContext( ConversionContext ctx );

    /**
     * Set node under to which converted content should be added
     *
     * @param  root  the root node for the conversion output
     */
    public void setTargetNode( javax.jcr.Node root );

}
