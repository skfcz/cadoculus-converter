/*
 * CADOculus, 3D in the Web
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the MIT Licens as published at
 * http://opensource.org/licenses/mit-license.php
 *
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the MIT Licens
 * along with this program.  If not, see <* http://opensource.org/licenses/mit-license.php>
 *
 */
package de.cadoculus.conversion;

import java.util.Properties;
import java.util.Set;


/**
 * The ConversionContext contains the context for the conversion. Most notably:
 *
 * <ul>
 *   <li>the intended target content. For the toplevel conversion it is always GEOMETRY_AND_SCENE.
 *     If a secondary conversion are triggered ( e.g. a STEP file referencing files with geometry
 *     contents), the targetformat is GEOMETRY_SCENE_ONLY_IF_FOUND. The secondary conversion should
 *     only created scenegraphs, if the referenced files contain both product structure. Otherwise
 *     only the geometry JSON has to be created.</li>
 *   <li>converted geometries, a set containing the JCR nodes containing geometry JSON</li>*
 *   <li>missing files, a set containing names of files referenced from a structure input file (
 *     e.g. STEP or assembly JT) which are not provided as JCR node to the conversion. To be set by
 *     the FileConverter after investigating the referenced file and finding a mismatch with
 *     uploaded files</li>
 *   <li>product structure, the JCR Node containing the toplevel scene graph after conversion</li>
 *   <li>conversion status, the status of the conversion</li>
 *   <li>uploaded files, the set of files available for conversion</li>
 *   <li>uploaded root file, the file marked as root file during upload. Conversion is started with
 *     this file</li>
 *   <li>targetFolder, the node under which converted files have to be created</li>
 * </ul>
 *
 * @author  cz
 */
public interface ConversionContext {

    /**
     * @return  the targetFolder
     */
    public javax.jcr.Node getTargetFolder();

    /**
     * @return  the attributes
     */
    Properties getAttributes();

    /**
     * The ConversionService implementation which could be run for secondary conversion
     *
     * @return  the service
     */
    ConversionService getConversionService();

    /**
     * @return  the convertedGeometry files
     */
    Set< javax.jcr.Node > getConvertedGeometries();

    /**
     * @return  the productStructure
     */
    javax.jcr.Node getConvertedRootStructure();

    /**
     * @return  the converted product structure files
     */
    Set< javax.jcr.Node > getConvertedStructures();

    /**
     * @return  the missingFiles
     */
    Set< String > getMissingFiles();

    /**
     * @return  the status
     */
    ConversionStatus getStatus();

    TargetContent getTargetContent();

    /**
     * @return  the uploadedFiles
     */
    Set< javax.jcr.Node > getUploadedFiles();

    /**
     * @return  the uploadedRootFile
     */
    javax.jcr.Node getUploadedRootFile();

  

    /**
     * @param  status  the status to set
     */
    void setStatus( ConversionStatus status );

}
