/*
 * CADOculus, 3D in the Web 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the MIT Licens as published at
 * http://opensource.org/licenses/mit-license.php
 * 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the MIT Licens
 * along with this program.  If not, see <* http://opensource.org/licenses/mit-license.php>
 *
 */
package de.cadoculus.conversion.threejs;

import java.io.PrintWriter;


/**
 * This class represents a geomnetry in the scenegraph.
 *
 * @author  cz
 */
public class ThreeJsBinMesh extends ThreeJsGeometry {
    private String relativePath;

    ThreeJsBinMesh( ThreeJsScene scene ) {
        super( scene, GeometryType.BIN_MESH );

    }

    public void toJSON( PrintWriter out ) {
        out.print( "    \"" );
        out.print( getId() );
        out.println( "\": {" );

        out.print( "        \"url\"   : \"" );
        out.print( getRelativePath() );
        out.println( "\"" );
        out.println( "     }," );
    }

    /**
     * @return  the relativePath
     */
    public String getRelativePath() {
        return relativePath;
    }

    /**
     * @param  relativePath  the relativePath to set
     */
    public void setRelativePath( String relativePath ) {
        this.relativePath = relativePath;
    }

}
