/*
 * CADOculus, 3D in the Web 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the MIT Licens as published at
 * http://opensource.org/licenses/mit-license.php
 * 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the MIT Licens
 * along with this program.  If not, see <* http://opensource.org/licenses/mit-license.php>
 *
 */
package de.cadoculus.conversion.threejs;

import java.io.PrintWriter;


/**
 * This class represents a geomnetry in the scenegraph.
 *
 * @author  cz
 */
public class ThreeJsSphere extends ThreeJsGeometry {
    private double radius = 1;

    private int segmentsHeight = 1;
    private int segmentsWidth = 1;

    ThreeJsSphere( ThreeJsScene scene ) {
        super( scene, GeometryType.SPHERE );

    }

    public void toJSON( PrintWriter out ) {
        out.println( "    \"sphere\": {" );
        out.println( "        \"type\"   : \"plane\"," );
        out.print( "        \"radius\"  :" );
        ThreeJsScene.format( out, getRadius() );
        out.println( "," );

        out.print( "        \"segmentsWidth\"  : " );
        ThreeJsScene.format( out, getSegmentsWidth() );
        out.println( "," );

        out.print( "        \"segmentsHeight\" :" );
        ThreeJsScene.format( out, getSegmentsHeight() );
        out.println( "," );
        out.println( "     }," );
    }

    /**
     * @return  the radius
     */
    public double getRadius() {
        return radius;
    }

    /**
     * @return  the segmentsHeight
     */
    public int getSegmentsHeight() {
        return segmentsHeight;
    }

    /**
     * @return  the segmentsWidth
     */
    public int getSegmentsWidth() {
        return segmentsWidth;
    }

    /**
     * @param  radius  the radius to set
     */
    public void setRadius( double radius ) {
        this.radius = radius;
    }

    /**
     * @param  segmentsHeight  the segmentsHeight to set
     */
    public void setSegmentsHeight( int segmentsHeight ) {
        this.segmentsHeight = segmentsHeight;
    }

    /**
     * @param  segmentsWidth  the segmentsWidth to set
     */
    public void setSegmentsWidth( int segmentsWidth ) {
        this.segmentsWidth = segmentsWidth;
    }

}
