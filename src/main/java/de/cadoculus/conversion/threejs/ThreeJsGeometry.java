/*
 * CADOculus, 3D in the Web 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the MIT Licens as published at
 * http://opensource.org/licenses/mit-license.php
 * 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the MIT Licens
 * along with this program.  If not, see <* http://opensource.org/licenses/mit-license.php>
 *
 */
package de.cadoculus.conversion.threejs;

import java.io.PrintWriter;
import java.util.UUID;


/**
 * This class represents a geomnetry in the scenegraph.
 *
 * @author  cz
 */
public abstract class ThreeJsGeometry {

    public enum GeometryType {
        PLANE, CUBE, SPHERE, CYLINDER, ICOSAHEDRON, TORUS, BIN_MESH, EMBEDDED_MESH
    }

    private final String id = UUID.randomUUID().toString();
    private final ThreeJsScene scene;

    private final GeometryType type;

    ThreeJsGeometry( ThreeJsScene scene, GeometryType type ) {
        this.type = type;
        this.scene = scene;

    }

    public abstract void toJSON( PrintWriter out );

    /**
     * Get the identifier for the geometry
     *
     * @return  the id
     */
    public String getId() {
        return id;
    }

    /**
     * Get the geometry type
     *
     * @return  the type
     */
    public GeometryType getType() {
        return type;
    }

}
