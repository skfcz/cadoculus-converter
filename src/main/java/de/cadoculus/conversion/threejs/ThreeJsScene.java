/*
 * CADOculus, 3D in the Web 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the MIT Licens as published at
 * http://opensource.org/licenses/mit-license.php
 * 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the MIT Licens
 * along with this program.  If not, see <* http://opensource.org/licenses/mit-license.php>
 *
 */
package de.cadoculus.conversion.threejs;

import de.cadoculus.conversion.threejs.ThreeJsGeometry.GeometryType;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import javax.vecmath.Tuple3d;


/**
 * @author  cz
 */
public class ThreeJsScene {

    private static Map< Integer, String > indentation = new HashMap< Integer, String >();
    private static final DecimalFormat DEC3 = new DecimalFormat( "0.0##",
            DecimalFormatSymbols.getInstance( Locale.ENGLISH ) );
    private static final double MIN = 1e-5;
    private Set< ThreeJsGeometry > geometries = new HashSet< ThreeJsGeometry >();
    private Set< ThreeJsMaterial > materials = new HashSet< ThreeJsMaterial >();
    private ThreeJsNode root = new ThreeJsNode( this, "root" );

    public < I extends ThreeJsGeometry > I addGeometry( Class< I > type ) {
        I retval = null;

        if ( ThreeJsPlane.class == type ) {
            retval = ( I ) new ThreeJsPlane( this );
        } else if ( ThreeJsSphere.class == type ) {
            retval = ( I ) new ThreeJsSphere( this );
        } else if ( ThreeJsCube.class == type ) {
            retval = ( I ) new ThreeJsCube( this );
        } else if ( ThreeJsBinMesh.class == type ) {
            retval = ( I ) new ThreeJsBinMesh( this );
        } else {
            throw new UnsupportedOperationException( "geometry type " + type +
                " not implemented yet" );
        }

        this.geometries.add( retval );

        return retval;

    }

    public ThreeJsNode addNode( ThreeJsNode parent, String name ) {
        ThreeJsNode retval = new ThreeJsNode( this, name );
        parent.getChildren().add( retval );

        return retval;
    }

    public void toJSON( OutputStream os ) throws IOException {
        PrintWriter out = new PrintWriter( os );
        out.println( "{" );
        out.println( "  \"metadata\": {" );
        out.println( "	  \"formatVersion\": 3," );
        out.println( "     \"type\" : \"scene\"" );
        out.println( "  }," );
        out.println();
        out.println( "  \"urlBaseType\" : \"relativeToHTML\"," );
        out.println();

        out.println( "  \"objects\": {" );
        root.toJSON( out, 0 );
        out.println( "\n}," );

        out.println( "  \"geometries\": {" );

        for ( Iterator< ThreeJsGeometry > it = geometries.iterator(); it.hasNext(); ) {
            ThreeJsGeometry threeJsGeometry = it.next();
            threeJsGeometry.toJSON( out );
            out.println( it.hasNext() ? "," : "" );
        }

        out.println( "\n}," );

        out.println( "  \"materials\": {" );

        for ( Iterator< ThreeJsMaterial > it = materials.iterator(); it.hasNext(); ) {
            ThreeJsMaterial threeJsMaterial = it.next();
            threeJsMaterial.toJSON( out );
            out.println( it.hasNext() ? "," : "" );

        }
        out.println( "\n}," );

        out.println( "\n}" );
        out.close();
    }

    public ThreeJsNode getRootNode() {
        return root;
    }

    static void format( PrintWriter out, double d ) {

        if ( Math.abs( d ) > MIN ) {
            out.print( DEC3.format( d ) );
        } else {
            out.print( "0.0" );
        }
    }

    static void format( PrintWriter out, Tuple3d p ) {
        out.print( "[ " );
        format( out, p.x );
        out.print( "," );
        format( out, p.y );
        out.print( "," );
        format( out, p.z );
        out.print( " ]" );

    }

    static String getIndent( int level ) {
        String indent = indentation.get( level );

        if ( indent == null ) {
            StringBuilder buff = new StringBuilder();

            for ( int i = 0; i <= level; i++ ) {
                buff.append( "    " );
            }

            indent = buff.toString();
            indentation.put( level, indent );
        }

        return indent;

    }
}
