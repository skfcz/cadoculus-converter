/*
 * CADOculus, 3D in the Web 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the MIT Licens as published at
 * http://opensource.org/licenses/mit-license.php
 * 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the MIT Licens
 * along with this program.  If not, see <* http://opensource.org/licenses/mit-license.php>
 *
 */
package de.cadoculus.conversion.threejs;

import java.io.PrintWriter;


/**
 * This class represents a geomnetry in the scenegraph.
 *
 * @author  cz
 */
public class ThreeJsPlane extends ThreeJsGeometry {
    private double height = 1;
    private int segmentsHeight = 1;
    private int segmentsWidth = 1;
    private double width = 1;

    ThreeJsPlane( ThreeJsScene scene ) {
        super( scene, GeometryType.PLANE );

    }

    public void toJSON( PrintWriter out ) {
        out.print( "    \"" );
        out.print( getId() );
        out.println( "\": {" );

        out.println( "        \"type\"   : \"plane\"," );
        out.print( "        \"width\"  :" );
        ThreeJsScene.format( out, getWidth() );
        out.println( "," );

        out.print( "        \"height\" : " );
        ThreeJsScene.format( out, getHeight() );
        out.println( "," );

        out.print( "        \"segmentsWidth\"  : " );
        ThreeJsScene.format( out, getSegmentsWidth() );
        out.println( "," );

        out.print( "        \"segmentsHeight\" :" );
        ThreeJsScene.format( out, getSegmentsHeight() );
        out.println( "," );
        out.println( "     }," );
    }

    /**
     * @return  the height
     */
    public double getHeight() {
        return height;
    }

    /**
     * @return  the segmentsHeight
     */
    public int getSegmentsHeight() {
        return segmentsHeight;
    }

    /**
     * @return  the segmentsWidth
     */
    public int getSegmentsWidth() {
        return segmentsWidth;
    }

    /**
     * @return  the width
     */
    public double getWidth() {
        return width;
    }

    /**
     * @param  height  the height to set
     */
    public void setHeight( double height ) {
        this.height = height;
    }

    /**
     * @param  segmentsHeight  the segmentsHeight to set
     */
    public void setSegmentsHeight( int segmentsHeight ) {
        this.segmentsHeight = segmentsHeight;
    }

    /**
     * @param  segmentsWidth  the segmentsWidth to set
     */
    public void setSegmentsWidth( int segmentsWidth ) {
        this.segmentsWidth = segmentsWidth;
    }

    /**
     * @param  width  the width to set
     */
    public void setWidth( double width ) {
        this.width = width;
    }

}
