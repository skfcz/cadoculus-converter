/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.cadoculus.conversion;

import javax.jcr.Node;
import javax.jcr.RepositoryException;


/**
 * The ConversionService is used to convert one file from the original format to the desired target
 * format. The ConversionService runs the file detection and conversion for the given file.
 *
 * @author  cz
 */
public interface ConversionService {

    /**
     * The command used to start the conversion.
     *
     * @throws  ConversionException  in case of errors.
     */
    public void convert( Node file2convert ) throws ConversionException, RepositoryException;

    /**
     * Get the ConversionContext used to as input and output container
     */
    public ConversionContext getConversionContext();

}
