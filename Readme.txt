This project contains some code for a converter framework 
intendend to convert CAD files (e.g. JT, STL, ...) to other formats.

Main target format is for a visualization using the Three.JS Javascript 
library.

The framework consists of a command line runner implementing a
container for the implementation and at least one demo converter.

To add your own converter one has to implement the interfaces 
FileTypeDetector and FileConverter.